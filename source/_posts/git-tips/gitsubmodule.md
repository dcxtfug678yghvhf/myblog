---
title: git（submodule）子模块详解
date: 2020-04-30 22:18:46
tags: 
 - git
 - git子模块
---
## 说明
在项目中使用其它的 git 仓库，这时需要用git submodule。

## 子模块的添加
命令：
```bash
git submodule add <url> <path>
```

## 子模块的使用
克隆项目后，默认子模块目录下无任何内容。需要在项目根目录执行如下命令完成子模块的下载：
```bash
git submodule init
git submodule update
或者：
git submodule update --init --recursive
```

## 删除子模块
有时子模块的项目维护地址发生了变化，或者需要替换子模块，就需要删除原有的子模块。

删除子模块较复杂，步骤如下：
rm -rf 子模块目录 删除子模块目录及源码
vi .gitmodules 删除项目目录下.gitmodules文件中子模块相关条目
vi .git/config 删除配置项中子模块相关条目
rm .git/module/* 删除模块下的子模块目录，每个子模块对应一个目录，注意只删除对应的子模块目录即可
git rm --cached 子模块名称

