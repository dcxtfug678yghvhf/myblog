---
title: PVE 中创建LXC实例
date: 2020-05-02 19:49:27
tags:
 - PVE
 - LXC容器
---

## 首先进入PVE控制台
推荐使用 [Mabaxterm](https://lanzous.com/i7u9tej) 连接到PVE服务器
## 下载 LXC 模板文件
在控制台输入：**pveam available --section system**列出系统模板
{% asset_img 2020-05-02_195531.png [LXC系统模板] %} 
然后下载模板：**pveam download local centos-7-default_20190926_amd64.tar.xz**

如果下载非常的慢可以使用下面这个地址，用迅雷。
http://download.proxmox.com/images/system/centos-7-default_20190926_amd64.tar.xz
然后再将文件上传PVE服务器**/var/lib/vz/template/cache/**目录下面

## 然后再在PVE的WebUI中创建LXC容器
{% asset_img 2020-05-02_200839.png [LXC系统模板] %} 
