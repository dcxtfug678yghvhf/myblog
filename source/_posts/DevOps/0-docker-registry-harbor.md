---
title: DevOps之Docker私仓搭建（harbor）
date: 2020-05-02 20:32:19
tags:
 - devops
---
## 先决条件：
### 安装 Docker CE
```bash
# centos 8.1 安装docker-ce
sudo yum install -y yum-utils 
sudo yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
sudo yum install docker-ce docker-ce-cli containerd.io --nobest
# 启动docker,并让它自启动
systemctl start docker
systemctl enable docker
```
### 添加docker国内镜像
```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://c1q062ot.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

### 安装docker-compose
下载 docker-compose {[传送门](https://github.com/docker/compose/releases)}

然后执行**chmod +x docker-compose**,并将文件移动到**/usr/local/bin**

## Harbor安装

### 下载 Harbor
打开地址:[官网](https://goharbor.io/)，[国内镜像](http://harbor.orientsoft.cn/)

### 解压并运行
```bash
tar xvf harbor-offline-installer-v1.5.0.tgz
```

### 生成证书
使用 [genca.sh](https://gitee.com/linvers/genca) 快速生成证书

### 证书配置：
```bash
cp yourdomain.com.crt /data/cert/
cp yourdomain.com.key /data/cert/

# 准备docker使用的证书（cert格式）
openssl x509 -inform PEM -in yourdomain.com.crt -out yourdomain.com.cert
# 复制到docker对应目录
cp yourdomain.com.cert /etc/docker/certs.d/yourdomain.com/
cp yourdomain.com.key /etc/docker/certs.d/yourdomain.com/
cp ca.crt /etc/docker/certs.d/yourdomain.com/
#重启docker
systemctl restart docker

```
如果你的https端口非443则目录是：** /etc/docker/certs.d/10.10.10.101:3999/**
If you mapped the default nginx port 443 to a different port, create the folder /etc/docker/certs.d/yourdomain.com:port, or /etc/docker/certs.d/harbor_IP:port.

### 配置域名和证书
```yml
# Configuration file of Harbor

# The IP address or hostname to access admin UI and registry service.
# DO NOT use localhost or 127.0.0.1, because Harbor needs to be accessed by external clients.
hostname: 10.10.10.20 #修改成你服务器的IP地址
# .............

# https related config
https:
  # https port for harbor, default is 443
  port: 443
  # The path of cert and key files for nginx
  certificate: /root/harbor/server-cert.crt
  private_key: /root/harbor/server-key.pem

```

### 运行**./prepare**

### 运行./install.sh 安装

### 关闭 harbor
docker-compose down -v

### 重启 harbor
docker-compose up -d
修改docker

## 重要
如果harbor提示密码不正确则需要 删除 /data下面所有的文件，然后重新进行 prepare和./install，则可以解决问题