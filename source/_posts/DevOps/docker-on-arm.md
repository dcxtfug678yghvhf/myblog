---
title: 在ARM架构(armhf)的CPU上使用Docker-平台cubietruck
date: 2020-05-15 22:49:28
tags:
- docker
- arm
---
本人在2013年时候买一块arm 开发板(cubietruck)，放在老家吃灰了非常多年，只到2018年才重新想起来，短暂的用来测试 openwrt。然后又吃一年多的灰。今天决定重新启用这个开发板，这块开发板总体运行时间不会超过72小时。电气元件的状态应该还是非常的好。今天决定用来安装一个 Linux 然后在上面运行 docker。以便获得在arm架构上使用docker的经验。
{% asset_img cubietruck.png [cubietruck] %} 

## 安装 Linux
因为这是比较老的armv7架构，它是32位的，所以现在的支持发行版（distro）比较少了，目前支持此开发板的distro有以下三个：
1. Armbian 这个是提供了两个版本（ubuntu,debian）[链接](https://www.armbian.com/ubietruck/)
2. Arch Linux Arm 这版本也非常的不错 [链接](https://archlinuxarm.org/platforms/armv7/allwinner/cubietruck)
3. Debian 官方的 ARM 版本 [链接](https://wiki.debian.org/InstallingDebianOn/Allwinner#Supported_Platforms)

当然后最版本还是 armbian 这个是开箱即用，本人使用的 armbian ubuntu。

### 首先要下载镜像
左边是 ubuntu ,右边是 debian
{% asset_img armbian-down.png [下载armbian] %} 

### 将镜像安装至SD卡中
准备一个至少8G大小的SD卡，本人SD卡是32G。并安装镜像写入工具 **BalenaEtcher**,[下载地址](https://www.balena.io/etcher/)。将镜像解压并写入SD卡中。
{% asset_img write-image.png [写入镜像] %} 

### 启动并配置 Armbian 国内镜像(bionic-ubuntu 18.04)

修改 /etc/apt/sources.list
```
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic-updates main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic-backports main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic-backports main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic-security main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic-security main restricted universe multiverse
```
修改 /etc/apt/source.d/armbian.list
```
deb http://mirrors.tuna.tsinghua.edu.cn/armbian/ bionic main bionic-utils bionic-desktop
```

### Armbian (debian版本更换镜像)
修改 /etc/apt/sources.list
```
deb http://mirrors.ustc.edu.cn/debian stretch main contrib non-free
deb http://mirrors.ustc.edu.cn/debian stretch-updates main contrib non-free
deb http://mirrors.ustc.edu.cn/debian stretch-backports main contrib non-free
deb http://mirrors.ustc.edu.cn/debian-security/ stretch/updates main contrib non-free
```

### 执行 **armbian-config** 命令更换镜像

**然后执行：apt update && apt upgrade**

{% asset_img welcom-armbian.png [写入镜像] %} 

## 安装 Docker for ARM
```bash
sudo apt-get install     apt-transport-https   ca-certificates \
     curl     gnupg-agent     software-properti es-common
curl -fsSL https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository    "deb [arch=armhf] https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu \
     $(lsb_release -cs) \
     stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

## 配置 docker hub 国内镜像
创建：/etc/docker/daemon.json
```json
{
  "registry-mirrors" : [
    "http://registry.docker-cn.com",
    "http://docker.mirrors.ustc.edu.cn",
    "http://hub-mirror.c.163.com"
  ],
  "insecure-registries" : [
    "registry.docker-cn.com",
    "docker.mirrors.ustc.edu.cn"
  ],
  "debug" : true,
  "experimental" : true
}
```

## docker 开机自动启动
systemctl enable docker





