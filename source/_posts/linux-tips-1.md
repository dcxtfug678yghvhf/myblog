---
title: Linux 下修改 grub2 的默认启动项
date: 2020-04-30 21:37:09
tags: 
 - linux
 - grub2默认启动项
---
1.打开 /etc/default/grub 文件找到，修改GRUB_DEFAULT=x，x为启动菜单中某项，比如启动菜单中第5条，这个x就为4，因为是从0开始记数。

2.执行：
```bash
 sudo update-grub
```

